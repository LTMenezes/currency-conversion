import Vue from 'vue'
import BootstrapVue from "bootstrap-vue"
import resource from "vue-resource"
import App from './App.vue'
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap-vue/dist/bootstrap-vue.css"
import './custom.css';

Vue.use(BootstrapVue)
Vue.use(resource)

var vm = new Vue({
  el: '#app',
  data: {
    message: ''
  },
  render: h => h(App)
})
