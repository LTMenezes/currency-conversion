# currency-conversion

A single page app for converting currencies.

## Demo

You can access it live in https://currency-conversion-ltmenezes.herokuapp.com/

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```
